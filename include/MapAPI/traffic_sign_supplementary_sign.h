/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_TRAFFICSIGNSUPPLEMENTARYSIGN_H
#define MAPSDK_MAPAPI_TRAFFICSIGNSUPPLEMENTARYSIGN_H

#include <string>
#include <vector>

#include "Common/common.h"
#include "lane.h"
#include "logical_lane_assignment.h"
#include "traffic_sign_common.h"

namespace map_api
{

enum class SupplementarySignType : uint8_t
{
  kUnknown = 0,
  kOther = 1,
  kNoSign = 2,
  kValidForDistance = 3,
  kValidInDistance = 4,
  kTimeRange = 5,
  kWeight = 6,
  kRain = 7,
  kFog = 8,
  kSnow = 9,
  kSnowRain = 10,
  kLeftArrow = 11,
  kRightArrow = 12,
  kLeftBendArrow = 13,
  kRightBendArrow = 14,
  kTruck = 15,
  kTractorsMayBePassed = 16,
  kHazardous = 17,
  kTrailer = 18,
  kNight = 19,
  kZone = 20,
  kStop_4Way = 21,
  kMotorcycle = 22,
  kMotorcycleAllowed = 23,
  kCar = 24,
  kStopIn = 25,
  kTime = 26,
  kPriorityRoadBottomLeftFourWay = 27,
  kPriorityRoadTopLeftFourWay = 28,
  kPriorityRoadBottomRightFourWay = 29,
  kArow = 30,
  kPriorityRoadTopRightFourWay = 31,
  kPriorityRoadBottomLeftThreeWayStraight = 32,
  kPriorityRoadBottomLeftThreeWaySideways = 33,
  kPriorityRoadTopLeftThreeWayStraight = 34,
  kPriorityRoadBottomRightThreeWayStraight = 35,
  kPriorityRoadBottomRightThreeWaySideway = 36,
  kPriorityRoadTopRightThreeWayStraight = 37,
  kNoWaitingSideStripes = 38,
  kSpace = 39,
  kAccident = 40,
  kText = 41,
  kParkingConstraint = 42,
  kParkingDiscTimeRestriction = 43,
  kWet = 44,
  kExcept = 45,
  kConstrainedTo = 46,
  kServices = 47,
  kRollingHighwayInformation = 48
};

enum class SupplementarySignActor : uint8_t
{
  kUnknown = 0,
  kOther = 1,
  kNoActor = 2,
  kAgriculturalVehicles = 3,
  kBicycles = 4,
  kBuses = 5,
  kCampers = 6,
  kCaravans = 7,
  kCars = 8,
  kCarsWithCaravans = 9,
  kCarsWithTrailers = 10,
  kCattle = 11,
  kChildren = 12,
  kConstructionVehicles = 13,
  kDeliveryVehicles = 14,
  kDisabledPersons = 15,
  kEbikes = 16,
  kElectricVehicles = 17,
  kEmergencyVehicles = 18,
  kFerryUsers = 19,
  kForestryVehicles = 20,
  kHazardousGoodsVehicles = 21,
  kHorseCarriages = 22,
  kHorseRiders = 23,
  kInlineSkaters = 24,
  kMedicalVehicles = 25,
  kMilitaryVehicles = 26,
  kMopeds = 27,
  kMotorcycles = 28,
  kMotorizedMultitrackVehicles = 29,
  kOperationalAndUtilityVehicles = 30,
  kPedestrians = 31,
  kPublicTransportVehicles = 32,
  kRailroadTraffic = 33,
  kResidents = 34,
  kSlurryTransport = 35,
  kTaxis = 36,
  kTractors = 37,
  kTrailers = 38,
  kTrams = 39,
  kTrucks = 40,
  kTrucksWithSemitrailers = 41,
  kTrucksWithTrailers = 42,
  kVehiclesWithGreenBadges = 43,
  kVehiclesWithRedBadges = 44,
  kVehiclesWithYellowBadges = 45,
  kWaterPollutantVehicles = 46,
  kWinterSportspeople = 47
};

struct SupplementarySignArrow
{
  enum class Direction : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNo_direction = 2,
    kDirect0Deg = 3,
    kDirect45DegRight = 4,
    kDirect45DegLeft = 5,
    kDirect90DegRight = 6,
    kDirect90DegLeft = 7,
    kDirect135DegRight = 8,
    kDirect135DegLeft = 9,
    kDirect180Deg = 10,
    kTurn45DegRight = 11,
    kTurn45DegLeft = 12,
    kTurn90DegRight = 13,
    kTurn90DegLeft = 14,
    kTurn135DegRight = 15,
    kTurn135DegLeft = 16,
    kTurn180DegRight = 17,
    kTurn180DegLeft = 18,
    kCircle0Deg = 19,
    kCircle45DegRight = 20,
    kCircle45DegLeft = 21,
    kCircle90DegRight = 22,
    kCircle90DegLeft = 23,
    kCircle135DegRight = 24,
    kCircle135DegLeft = 25,
    kCircle180Deg = 26,
    kKeepLeftToTurn0Deg = 27,
    kKeepRightToTurn0Deg = 28,
    kKeepLeftToTurn90DegRight = 29,
    kKeepRightToTurn90DegLeft = 30,
    kKeepLeftDriveBackToTurn90DegRight = 31,
    kKeepRightDriveBackToTurn90DegLeft = 32
  };
  std::vector<RefWrapper<Lane>> lanes;
  std::vector<Direction> direction;
};

struct SupplementarySign
{
  BaseProperties base;
  TrafficSignVariability variability;
  SupplementarySignType type;
  std::vector<TrafficSignValue> values;
  std::vector<RefWrapper<Lane>> assigned_lanes;
  std::vector<SupplementarySignActor> actors;
  std::vector<SupplementarySignArrow> arrows;
  bool is_out_of_service;
  std::string country;
  std::string country_revision;
  std::string code;
  std::string sub_code;
  std::vector<LogicalLaneAssignment> logical_lane_assignments;
  std::string model_reference;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_TRAFFICSIGNSUPPLEMENTARYSIGN_H
