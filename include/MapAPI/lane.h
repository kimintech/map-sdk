/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_LANE_H
#define MAPSDK_MAPAPI_LANE_H

#include <vector>

#include "Common/common.h"
#include "lane_boundary.h"

namespace map_api
{
struct Lane;

struct RoadCondition
{
  double surface_temperature;
  double surface_water_film;
  double surface_freezing_point;
  double surface_ice;
  double surface_roughness;
  double surface_texture;
};

struct Lane
{
  using PolylinePoint = Vector3d;
  using Polyline = std::vector<PolylinePoint>;

  enum class Type : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kDriving = 2,
    kNonDriving = 3,
    kIntersection = 4
  };

  enum class Subtype : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNormal = 2,
    kBiking = 3,
    kSidewalk = 4,
    kParking = 5,
    kStop = 6,
    kRestricted = 7,
    kBorder = 8,
    kShoulder = 9,
    kExit = 10,
    kEntry = 11,
    kOnRamp = 12,
    kOffRamp = 13,
    kConnectingRamp = 14
  };

  Identifier id{UndefinedId};
  Type type;
  Subtype sub_type;

  /// @note: ordered with the driving direction
  Polyline centerline;

  std::vector<RefWrapper<Lane>> left_adjacent_lanes;
  std::vector<RefWrapper<Lane>> right_adjacent_lanes;
  std::vector<RefWrapper<Lane>> successor_lanes;
  std::vector<RefWrapper<Lane>> antecessor_lanes;

  std::vector<RefWrapper<LaneBoundary>> left_lane_boundaries;
  std::vector<RefWrapper<LaneBoundary>> right_lane_boundaries;
  std::vector<RefWrapper<LaneBoundary>> free_lane_boundaries;

  RoadCondition road_condition;

  std::vector<ExternalReference> source_references;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_LANE_H
