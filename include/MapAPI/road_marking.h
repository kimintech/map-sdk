/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_ROADMARKING_H
#define MAPSDK_MAPAPI_ROADMARKING_H

#include "Common/common.h"
#include "lane.h"
#include "logical_lane_assignment.h"
#include "traffic_sign_common.h"
#include "traffic_sign_main_sign.h"

namespace map_api
{

struct RoadMarking
{
  enum class Color : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kWhite = 2,
    kYellow = 3,
    kBlue = 4,
    kRed = 5,
    kGreen = 6,
    kViolet = 7,
    kOrange = 8
  };

  enum class Type : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kPaintedTrafficSign = 2,
    kSymbolicTrafficSign = 3,
    kTextualTrafficSign = 4,
    kGenericSymbol = 5,
    kGenericLine = 6,
    kGenericText = 7
  };
  Identifier id{UndefinedId};
  BaseProperties base;

  Type type;
  MainSignType traffic_main_sign_type;
  Color monochrome_color;
  TrafficSignValue value;
  std::string value_text;
  bool is_out_of_service;
  std::string country;
  std::string country_revision;
  std::string code;
  std::string sub_code;

  std::vector<RefWrapper<Lane>> assigned_lanes;
  std::vector<LogicalLaneAssignment> logical_lane_assignments;

  std::vector<ExternalReference> source_references;
};
}  // namespace map_api

#endif  // MAPSDK_MAPAPI_ROADMARKING_H
