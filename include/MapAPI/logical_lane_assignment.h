/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_COMMON_LOGICALLANEASSIGNMENT_H
#define MAPSDK_MAPAPI_COMMON_LOGICALLANEASSIGNMENT_H

#include <units.h>

#include "logical_lane.h"

namespace map_api
{
struct LogicalLaneAssignment
{
  LogicalLane& assigned_lane;
  units::length::meter_t s_position;
  units::length::meter_t t_position;
  units::angle::radian_t angle_to_lane;
};
}  // namespace map_api

#endif  // MAPSDK_MAPAPI_COMMON_LOGICALLANEASSIGNMENT_H
