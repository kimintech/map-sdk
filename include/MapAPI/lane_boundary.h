/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_LANEBOUNDARY_H
#define MAPSDK_MAPAPI_LANEBOUNDARY_H

#include <units.h>

#include <vector>

#include "Common/common.h"

namespace map_api
{

struct StationaryObject;

struct LaneBoundary
{
  struct BoundaryPoint
  {
    enum class Dash : std::uint8_t
    {
      kUnknown = 0,
      kOther = 1,
      kStart = 2,
      kContinue = 3,
      kEnd = 4,
      kGap = 5
    };

    Position3d position;
    units::length::meter_t width;
    units::length::meter_t height;
    Dash dash;
  };

  enum class Color : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNone = 2,
    kWhite = 3,
    kYellow = 4,
    kRed = 5,
    kBlue = 6,
    kGreen = 7,
    kViolet = 8,
    kOrange = 9
  };

  enum class Type : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNoLine = 2,
    kSolidLine = 3,
    kDashedLine = 4,
    kBottsDots = 5,
    kRoadEdge = 6,
    kSnowEdge = 7,
    kGrassEdge = 8,
    kGravelEdge = 9,
    kSoilEdge = 10,
    kGuardRail = 11,
    kCurb = 12,
    kStructure = 13,
    kBarrier = 14,
    kSoundBarrier = 15
  };

  Identifier id{UndefinedId};
  std::vector<BoundaryPoint> boundary_line;
  Type type;
  Color color;

  std::vector<RefWrapper<StationaryObject>> limiting_structures;
  std::vector<ExternalReference> source_references;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_LANEBOUNDARY_H
