/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_COMMON_GEOMETRY_H
#define MAPSDK_MAPAPI_COMMON_GEOMETRY_H

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>

#include "identifier.h"

namespace map_api
{

using Vector3d = mantle_api::Vec3<units::length::meter_t>;
using Dimension3d = mantle_api::Dimension3;
using Orientation3d = mantle_api::Orientation3<units::angle::radian_t>;
using Position3d = Vector3d;

}  // namespace map_api

#endif
