/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_COMMON_IDENTIFIER_H
#define MAPSDK_MAPAPI_COMMON_IDENTIFIER_H

#include <cstdint>

namespace map_api
{

using Identifier = std::uint64_t;
constexpr Identifier UndefinedId{std::numeric_limits<Identifier>::max()};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_COMMON_IDENTIFIER_H
