/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_LOGICALLANE_H
#define MAPSDK_MAPAPI_LOGICALLANE_H

#include <units.h>

#include <vector>

#include "Common/common.h"
#include "lane.h"
#include "logical_lane_boundary.h"
#include "reference_line.h"

namespace map_api
{

struct LogicalLane;

struct PhysicalLaneReference
{
  Lane& physical_lane;
  units::length::meter_t start_s;
  units::length::meter_t end_s;
};

struct LogicalLaneRelation
{
  LogicalLane& other_lane;
  units::length::meter_t start_s;
  units::length::meter_t end_s;
  units::length::meter_t start_s_other;
  units::length::meter_t end_s_other;
};

struct LogicalLane
{
  enum class Type : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNormal = 2,
    kBiking = 3,
    kSidewalk = 4,
    kParking = 5,
    kStop = 6,
    kRestricted = 7,
    kBorder = 8,
    kShoulder = 9,
    kExit = 10,
    kEntry = 11,
    kOnramp = 12,
    kOfframp = 13,
    kConnectingramp = 14,
    kMedian = 15,
    kCurb = 16,
    kRail = 17,
    kTram = 18
  };

  /// @ref osi3::LogicalLane::MoveDirection
  /// @see \link https://github.com/OpenSimulationInterface/open-simulation-interface/tree/v3.5.0 \endlink
  enum class MoveDirection : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kIncreasingS = 2,
    kDecreasingS = 3,
    kBothAllowed = 4
  };

  Identifier id{UndefinedId};
  units::length::meter_t start_s;
  units::length::meter_t end_s;

  ReferenceLine* reference_line;

  Type type;

  std::vector<ExternalReference> source_references;
  std::vector<PhysicalLaneReference> physical_lane_references;

  MoveDirection move_direction;
  std::vector<LogicalLaneRelation> right_adjacent_lanes;
  std::vector<LogicalLaneRelation> left_adjacent_lanes;
  std::vector<LogicalLaneRelation> overlapping_lanes;
  std::vector<RefWrapper<LogicalLaneBoundary>> right_boundaries;
  std::vector<RefWrapper<LogicalLaneBoundary>> left_boundaries;

  std::vector<RefWrapper<LogicalLane>> predecessor_lanes;
  std::vector<RefWrapper<LogicalLane>> successor_lanes;


};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_LOGICALLANE_H
