/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "MapAPI/map.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace
{

using units::literals::operator""_m;
using namespace map_api;

///////////////////////////////////////////////////////////
/// @verbatim
/// Connected Lanes
/// IDs:
/// |-----------------------------|-----------------------------| y = 4.5
/// |    Left LaneBoundary:  100  |    Left LaneBoundary:  103  |
/// |    Lane: 0                  |    Lane: 2                  | y = 3.0
/// |    Right LaneBoundary: 101  |    Right LaneBoundary: 104  |
/// |-----------------------------|-----------------------------| y = 1.5
/// |    Left LaneBoundary:  101  |    Left LaneBoundary:  104  |
/// |    Lane: 1                  |    Lane: 3                  | y = 0.0
/// |    Right LaneBoundary: 102  |    Right LaneBoundary: 105  |
/// |-----------------------------|-----------------------------| y = -1.5
/// ^                             ^                             ^
/// x(0)                         (1000)                       (2000)
///
/// @endverbatim

inline auto CreateLaneBoundary(Identifier id, const units::length::meter_t start_x, const units::length::meter_t y_offset)
{
  auto lane_boundary = std::make_unique<LaneBoundary>();
  lane_boundary->id = id;
  lane_boundary->boundary_line = std::vector<LaneBoundary::BoundaryPoint>{
      {.position = {.x = start_x, .y = y_offset, .z = 0.0_m}, .width = 0.13_m, .height = 0.05_m},
      {.position = {.x = start_x + 1000.0_m, .y = y_offset, .z = 0.0_m}, .width = 0.13_m, .height = 0.05_m}};
  lane_boundary->type = LaneBoundary::Type::kSolidLine;
  lane_boundary->color = LaneBoundary::Color::kWhite;
  return std::move(lane_boundary);
}

inline auto CreateLane(Identifier id, const units::length::meter_t start_x, const units::length::meter_t y_offset)
{
  auto lane = std::make_unique<Lane>();
  lane->id = id;
  lane->type = Lane::Type::kDriving;
  lane->sub_type = Lane::Subtype::kUnknown;
  lane->centerline = Lane::Polyline{
      Lane::PolylinePoint{start_x, y_offset, 0.0_m},
      Lane::PolylinePoint{start_x + 1000.0_m, y_offset, 0.0_m}};

  return std::move(lane);
}

inline Map CreateMapWithConnectedLanes()
{
  Map map{};
  map.projection_string = "+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs";

  auto lane_boundary_100 = CreateLaneBoundary(100, 0.0_m, 4.5_m);
  auto lane_boundary_101 = CreateLaneBoundary(101, 0.0_m, 1.5_m);
  auto lane_boundary_102 = CreateLaneBoundary(102, 0.0_m, -1.5_m);
  auto lane_boundary_103 = CreateLaneBoundary(103, 1000.0_m, 4.5_m);
  auto lane_boundary_104 = CreateLaneBoundary(104, 1000.0_m, 1.5_m);
  auto lane_boundary_105 = CreateLaneBoundary(105, 1000.0_m, -1.5_m);

  auto lane_0 = CreateLane(0, 0.0_m, 3.0_m);
  auto lane_1 = CreateLane(1, 0.0_m, 3.0_m);
  auto lane_2 = CreateLane(2, 1000.0_m, 3.0_m);
  auto lane_3 = CreateLane(3, 1000.0_m, 0.0_m);

  lane_0->left_lane_boundaries.push_back(*lane_boundary_100);
  lane_0->right_lane_boundaries.push_back(*lane_boundary_101);
  lane_0->right_adjacent_lanes.push_back(*lane_1);
  lane_0->successor_lanes.push_back(*lane_2);

  lane_1->left_lane_boundaries.push_back(*lane_boundary_101);
  lane_1->right_lane_boundaries.push_back(*lane_boundary_102);
  lane_1->left_adjacent_lanes.push_back(*lane_0);
  lane_1->successor_lanes.push_back(*lane_3);

  lane_2->left_lane_boundaries.push_back(*lane_boundary_103);
  lane_2->right_lane_boundaries.push_back(*lane_boundary_104);
  lane_2->right_adjacent_lanes.push_back(*lane_3);
  lane_2->antecessor_lanes.push_back(*lane_0);

  lane_3->left_lane_boundaries.push_back(*lane_boundary_104);
  lane_3->right_lane_boundaries.push_back(*lane_boundary_105);
  lane_3->left_adjacent_lanes.push_back(*lane_2);
  lane_3->antecessor_lanes.push_back(*lane_1);

  map.lanes.emplace_back(std::move(lane_0));
  map.lanes.emplace_back(std::move(lane_1));
  map.lanes.emplace_back(std::move(lane_2));
  map.lanes.emplace_back(std::move(lane_3));
  map.lane_boundaries.emplace_back(std::move(lane_boundary_100));
  map.lane_boundaries.emplace_back(std::move(lane_boundary_101));
  map.lane_boundaries.emplace_back(std::move(lane_boundary_102));
  map.lane_boundaries.emplace_back(std::move(lane_boundary_103));
  map.lane_boundaries.emplace_back(std::move(lane_boundary_104));
  map.lane_boundaries.emplace_back(std::move(lane_boundary_105));

  map.logical_lanes.emplace_back(std::make_unique<LogicalLane>());
  map.logical_lane_boundaries.emplace_back(std::make_unique<LogicalLaneBoundary>());
  map.traffic_lights.emplace_back(std::make_unique<TrafficLight>());
  map.traffic_signs.emplace_back(std::make_unique<TrafficSign>());
  map.reference_lines.emplace_back(std::make_unique<ReferenceLine>());
  map.road_markings.emplace_back(std::make_unique<RoadMarking>());
  map.stationary_objects.emplace_back(std::make_unique<StationaryObject>());

  map.lane_boundaries.front()->limiting_structures.push_back(*(map.stationary_objects.front()));

  return map;
}

TEST(MapTest, GivenEmptyMap_WhenInitialized_ThenDefaultValuesAreSet)
{
  Map map;

  EXPECT_EQ(map.projection_string, "");
  EXPECT_EQ(map.map_reference, "");
  EXPECT_EQ(map.country_code, std::numeric_limits<std::uint32_t>::max());
  EXPECT_TRUE(map.lanes.empty());
  EXPECT_TRUE(map.lane_boundaries.empty());
  EXPECT_TRUE(map.logical_lanes.empty());
  EXPECT_TRUE(map.logical_lane_boundaries.empty());
  EXPECT_TRUE(map.stationary_objects.empty());
  EXPECT_TRUE(map.traffic_lights.empty());
  EXPECT_TRUE(map.traffic_signs.empty());
  EXPECT_TRUE(map.reference_lines.empty());
  EXPECT_TRUE(map.road_markings.empty());
}

TEST(MapTest, GivenMapWithConnectedLanes_WhenInitialized_ThenValuesAreSet)
{
  const auto map = CreateMapWithConnectedLanes();

  EXPECT_EQ(map.projection_string, "+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
  EXPECT_EQ(map.map_reference, "");
  EXPECT_EQ(map.country_code, std::numeric_limits<std::uint32_t>::max());
  EXPECT_EQ(map.lanes.size(), 4);
  EXPECT_EQ(map.lane_boundaries.size(), 6);
  EXPECT_EQ(map.logical_lanes.size(), 1);
  EXPECT_EQ(map.logical_lane_boundaries.size(), 1);
  EXPECT_EQ(map.stationary_objects.size(), 1);
  EXPECT_EQ(map.traffic_lights.size(), 1);
  EXPECT_EQ(map.traffic_signs.size(), 1);
  EXPECT_EQ(map.reference_lines.size(), 1);
  EXPECT_EQ(map.road_markings.size(), 1);
}

TEST(MapTest, GivenMapWithConnectedLanes_WhenInitialized_ThenLaneBoundariesAreSetCorrectly)
{
  const auto map = CreateMapWithConnectedLanes();
  ASSERT_EQ(map.lanes.size(), 4);

  auto& lane_0 = map.lanes.at(0);
  ASSERT_EQ(lane_0->left_lane_boundaries.size(), 1);
  ASSERT_EQ(lane_0->right_lane_boundaries.size(), 1);
  EXPECT_EQ(lane_0->left_lane_boundaries.front().get().id, 100);
  EXPECT_EQ(lane_0->right_lane_boundaries.front().get().id, 101);

  auto& lane_1 = map.lanes.at(1);
  ASSERT_EQ(lane_1->left_lane_boundaries.size(), 1);
  ASSERT_EQ(lane_1->right_lane_boundaries.size(), 1);
  EXPECT_EQ(lane_1->left_lane_boundaries.front().get().id, 101);
  EXPECT_EQ(lane_1->right_lane_boundaries.front().get().id, 102);

  auto& lane_2 = map.lanes.at(2);
  ASSERT_EQ(lane_2->left_lane_boundaries.size(), 1);
  ASSERT_EQ(lane_2->right_lane_boundaries.size(), 1);
  EXPECT_EQ(lane_2->left_lane_boundaries.front().get().id, 103);
  EXPECT_EQ(lane_2->right_lane_boundaries.front().get().id, 104);

  auto& lane_3 = map.lanes.at(3);
  ASSERT_EQ(lane_3->left_lane_boundaries.size(), 1);
  ASSERT_EQ(lane_3->right_lane_boundaries.size(), 1);
  EXPECT_EQ(lane_3->left_lane_boundaries.front().get().id, 104);
  EXPECT_EQ(lane_3->right_lane_boundaries.front().get().id, 105);
}

TEST(MapTest, GivenMapWithConnectedLanes_WhenInitialized_ThenSuccessorAndAntecessorLanesAreSetCorrectly)
{
  const auto map = CreateMapWithConnectedLanes();
  ASSERT_EQ(map.lanes.size(), 4);

  auto& lane_0 = map.lanes.at(0);
  auto& lane_1 = map.lanes.at(1);
  auto& lane_2 = map.lanes.at(2);
  auto& lane_3 = map.lanes.at(3);

  ASSERT_EQ(lane_0->successor_lanes.size(), 1);
  ASSERT_EQ(lane_0->antecessor_lanes.size(), 0);
  EXPECT_EQ(lane_0->successor_lanes.front().get().id, lane_2->id);

  ASSERT_EQ(lane_1->successor_lanes.size(), 1);
  ASSERT_EQ(lane_1->antecessor_lanes.size(), 0);
  EXPECT_EQ(lane_1->successor_lanes.front().get().id, lane_3->id);

  ASSERT_EQ(lane_2->successor_lanes.size(), 0);
  ASSERT_EQ(lane_2->antecessor_lanes.size(), 1);
  EXPECT_EQ(lane_2->antecessor_lanes.front().get().id, lane_0->id);

  ASSERT_EQ(lane_3->successor_lanes.size(), 0);
  ASSERT_EQ(lane_3->antecessor_lanes.size(), 1);
  EXPECT_EQ(lane_3->antecessor_lanes.front().get().id, lane_1->id);
}

TEST(MapTest, GivenMapWithConnectedLanes_WhenInitialized_ThenLeftAndRightAdjacentLaneAreSetCorrectly)
{
  const auto map = CreateMapWithConnectedLanes();
  ASSERT_EQ(map.lanes.size(), 4);

  auto& lane_0 = map.lanes.at(0);
  auto& lane_1 = map.lanes.at(1);
  auto& lane_2 = map.lanes.at(2);
  auto& lane_3 = map.lanes.at(3);

  ASSERT_EQ(lane_0->left_adjacent_lanes.size(), 0);
  ASSERT_EQ(lane_0->right_adjacent_lanes.size(), 1);
  EXPECT_EQ(lane_0->right_adjacent_lanes.front().get().id, lane_1->id);

  ASSERT_EQ(lane_1->left_adjacent_lanes.size(), 1);
  ASSERT_EQ(lane_1->right_adjacent_lanes.size(), 0);
  EXPECT_EQ(lane_1->left_adjacent_lanes.front().get().id, lane_0->id);

  ASSERT_EQ(lane_2->left_adjacent_lanes.size(), 0);
  ASSERT_EQ(lane_2->right_adjacent_lanes.size(), 1);
  EXPECT_EQ(lane_2->right_adjacent_lanes.front().get().id, lane_3->id);

  ASSERT_EQ(lane_3->left_adjacent_lanes.size(), 1);
  ASSERT_EQ(lane_3->right_adjacent_lanes.size(), 0);
  EXPECT_EQ(lane_3->left_adjacent_lanes.front().get().id, lane_2->id);
}

}  // namespace
